### **Docker Summary**

**1. Docker** is a programme for developers to run applications using containers.  
![Docker logo](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)

#### **2. Terminologies:**  
 
- Docker Image: Contains code, libraries, environment variables and configuration files required to run an application.  
- Docker Container: It is a running docker image,can create multiple containers from one image.  
- Docker Hub: This hosts all docker images and containers.  

#### **3. Docker Engine:**  
 Docker Engine is the underlying client-server technology that builds and runs containers using Docker's components and services.  
 ![Docker Engine](https://docs.docker.com/engine/images/engine-components-flow.png)  


#### **4. Docker Installation:**  
Step-by-step procedure to install docker on Ubuntu 16.04  
[Installation link](https://docs.docker.com/engine/install/ubuntu/)

#### **5. Docker Commands:**
 
- ps- view all containers running on docker hosts.  
  `docker ps`  
 
- start- starts any stopped containers.  
  `docker start <container-id>`  
 
- stop- stops running containers.  
  `docker stop <container-id>`  
 
- run- run- creates containers from docker images.  
  `docker run <container-id>`  
 
- rm- deletes docker containers.  
  `docker rm <container-id>`  

#### **6. Docker Operations:**  
 - Download the docker  
  `docker pull <docker address>`  

- Run the docker image  
 `docker run -ti <docker address/folder name>`  
 output contains the docker container id  

- Copy code inside docker  
  `docker cp <program-file> <container-id>`  

- Give permission to run shell scripts  
  `docker exec -it <container-id> chmod <script>`  

- Install dependencies  
  `docker exec -it <docker address/.../script>`  

- Run program inside container  
  `docker exec <container-id> <language> <program-file>` 

- Save copied program using commit  
  `docker commit <container-id> <docker-address>`  

- Push docker image to docker hub  
  `docker push username/repo`